import os
from rss.models import db
from flask import Flask, request, session
from listenup_common.debugger import start_debug
from listenup_common.logging import get_logger
from flask_migrate import Migrate
from celery import Celery
from config import config

from rss import models

logger = get_logger(__name__)
migrate = Migrate()

celery_broker = 'redis://:' + os.environ.get('REDIS_PASSWORD') + '@' + os.environ.get('REDIS') + '/4'

# TODO remove this it will print the password in plain text...
logger.debug(celery_broker)

celery = Celery(__name__,
                broker=celery_broker,
                backend=celery_broker)
celery.config_from_object('celeryconfig')

if os.environ.get("DEBUG", False):
    start_debug(12346)

from rss import tasks


def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')
        logger.info("Starting up in: %s", config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.host = '0.0.0.0'

    celery.conf.update(config[config_name].CELERY_CONFIG)

    # Initialize flask extensions
    db.init_app(app)
    migrate.init_app(app=app, db=db)

    # Register web application routes
    from .routes import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
