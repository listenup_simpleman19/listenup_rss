from typing import List
from flask import current_app as app
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from listenup_common.utils import guid
import uuid

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False,
                                   default=datetime.utcnow)


class RssFeed(BaseModel):
    __tablename__ = 'rss_feed'
    created_by_user_guid = db.Column(db.String(32), nullable=False)
    feed_url = db.Column(db.String(400), nullable=False, unique=True)
    feed_guid = db.Column(db.String(32), nullable=False, unique=True)
    image_guid = db.Column(db.String(32), nullable=True)

    # These appear to be required in the standard
    title = db.Column(db.String(400), nullable=True)
    description = db.Column(db.String(10000), nullable=True)
    link = db.Column(db.String(400), nullable=True)
    # These are optional
    language = db.Column(db.String(100), nullable=True)
    category = db.Column(db.String(500), nullable=True)
    pub_date = db.Column(db.DateTime, nullable=True)
    last_build_date = db.Column(db.DateTime, nullable=True)

    image_url = db.Column(db.String(400), nullable=True)
    image_title = db.Column(db.String(400), nullable=True)
    image_link = db.Column(db.String(400), nullable=True)

    itunes_subtitle = db.Column(db.String(10000), nullable=True)
    itunes_image_url = db.Column(db.String(400))
    itunes_author = db.Column(db.String(400))

    def __init__(self, feed_url, user_guid=None):
        self.feed_guid = guid()
        self.feed_url = feed_url
        self.created_by_user_guid = user_guid

    def to_dict(self, channel_style: bool = False, podcasts: List = None) -> dict:
        if channel_style:
            channel = {
                'id': self.id,
                'creation_timestamp': self.creation_timestamp,
                'feed_url': self.feed_url,
                'feed_guid': self.feed_guid,
                'guid': self.feed_guid,
                'image_guid': self.image_guid,
                'title': self.title,
                'description': self.description,
                'link': self.link,
                'language': self.language,
                'category': self.category,
                'pub_date': self.pub_date,
                'last_build_date': self.last_build_date,
                'image_url': self.image_url,
                'image_title': self.image_title,
                'itunes_subtitle': self.itunes_subtitle,
                'itunes_image_url': self.itunes_image_url,
                'itunes_author': self.itunes_author,
                'rss_feed': True
            }
            if podcasts:
                channel['podcasts'] = podcasts
            return channel
        else:
            return {
                'id': self.id,
                'creation_timestamp': self.creation_timestamp,
                'feed_url': self.feed_url,
                'feed_guid': self.feed_guid,
                'guid': self.feed_guid,
                'image_guid': self.image_guid,
                'title': self.title,
                'description': self.description,
                'link': self.link,
                'language': self.language,
                'category': self.category,
                'pub_date': self.pub_date,
                'last_build_date': self.last_build_date,
                'image_url': self.image_url,
                'image_title': self.image_title,
                'itunes_subtitle': self.itunes_subtitle,
                'itunes_image_url': self.itunes_image_url,
                'itunes_author': self.itunes_author,
                'rss_feed': True
            }


class RssItem(BaseModel):
    __tablename_ = 'rss_item'
    feed_id = db.Column(db.Integer, nullable=False)
    feed_guid = db.Column(db.String(32), nullable=False)
    item_guid = db.Column(db.String(32), nullable=False)
    title = db.Column(db.String(400), nullable=True)
    author = db.Column(db.String(400), nullable=True)
    description = db.Column(db.String(10000), nullable=True)
    link = db.Column(db.String(400), nullable=True)
    language = db.Column(db.String(100), nullable=True)
    pub_date = db.Column(db.DateTime, nullable=True)
    last_build_date = db.Column(db.DateTime, nullable=True)
    itunes_subtitle = db.Column(db.String(400), nullable=True)
    itunes_image_url = db.Column(db.String(400), nullable=True)
    itunes_summary = db.Column(db.String(10000), nullable=True)
    itunes_duration = db.Column(db.String(100), nullable=True)
    guid = db.Column(db.String(400), nullable=True)
    source = db.Column(db.String(400), nullable=True)
    source_href = db.Column(db.String(500), nullable=True)
    # Required due to need for actual podcast file
    enclosure_url = db.Column(db.String(500), nullable=False, unique=True)
    enclosure_mime_type = db.Column(db.String(100), nullable=False)
    length = db.Column(db.Integer, nullable=False)
    podcast_guid = db.Column(db.String(32), nullable=False)

    def __init__(self, feed: RssFeed):
        self.item_guid = guid()
        self.feed_id = feed.id
        self.feed_guid = feed.feed_guid

    def to_dict(self, podcast=None):
        item_dict = {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'feed_id': self.feed_id,
            'feed_guid': self.feed_guid,
            'item_guid': self.item_guid,
            'title': self.title,
            'description': self.description,
            'link': self.link,
            'language': self.language,
            'pub_date': self.pub_date,
            'last_build_date': self.last_build_date,
            'itunes_subtitle': self.itunes_subtitle,
            'itunes_image_url': self.itunes_image_url,
            'rss_feed': True,
            'podcast_guid': self.podcast_guid,
        }

        if podcast:
            item_dict['podcast'] = podcast
        return item_dict


class RssItemFile(BaseModel):
    __tablename__ = 'rss_item_file'
    item_id = db.Column(db.Integer, nullable=False)
    item_guid = db.Column(db.String(32), nullable=False)
    file_guid = db.Column(db.String(32), nullable=False)
    mime_type = db.Column(db.String(100), nullable=True)
    quality = db.Column(db.String(100), nullable=True)

    def __init__(self, item: RssItem, file_guid: str):
        self.item_id = item.id
        self.item_guid = item.item_guid
        self.file_guid = file_guid

    def to_dict(self):
        return {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'item_guid': self.item_guid,
            'file_guid': self.file_guid,
            'mime_type': self.mime_type,
            'quality': self.quality,
        }
