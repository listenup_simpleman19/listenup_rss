from typing import Optional

from rss import celery, db
from rss.models import RssFeed, RssItem, RssItemFile
from listenup_common.logging import get_logger
from listenup_common.wrappers.files import download_file
from listenup_common.wrappers.podcast import create_podcast, get_podcast_info, upload_podcast
from listenup_common.wrappers.channel import get_channel_by_guid
import feedparser
from time import mktime
from datetime import datetime
from flask import current_app
import requests
import uuid
import os

RETRY_TIMES = 5

config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')

logger = get_logger(__name__)


@celery.task
def update_feed(channel_guid):
    from rss import create_app
    app = create_app(config_name)
    with app.app_context():
        logger.info("Updating feed channel: " + str(channel_guid))
        parse_feed(channel_guid)


def parse_feed(channel_guid):
    found_feed = False
    retry = 0
    channel = get_channel_by_guid(guid=channel_guid)
    while not found_feed and retry < RETRY_TIMES:
        retry += 1
        logger.debug("Parsing feed: %s retry: %s", channel.get("feed_url"), str(retry))
        parsed_feed = feedparser.parse(channel.get("feed_url"))
        if 'feed' in parsed_feed:
            act_feed = parsed_feed['feed']
            if 'title' in act_feed:
                found_feed = True
                logger.debug(f"Parsing feed: {act_feed}")
                json = {
                    "title": act_feed.get('title'),
                    "description": act_feed.get('description'),
                    "category": act_feed.get("category"),
                    "itunes_subtitle": act_feed.get("subtitle"),
                }
                if "image" in act_feed:
                    json['image_url'] = act_feed.get("image").get("href")
                    process_image(json)
                if act_feed.get("published_parsed"):
                    json['pub_date'] = datetime.fromtimestamp(mktime(act_feed.get("published_parsed")))
                if act_feed.get("updated_parsed"):
                    json['last_build_date'] = datetime.fromtimestamp(mktime(act_feed.get("updated_parsed")))
        if len(parsed_feed.get('entries', [])) > 0:
            logger.debug(f"Parsing items: {parsed_feed['entries']}")
            # for item in parsed_feed['entries']:
            #     parse_item(feed, item)
            db.session.commit()


def parse_item(feed, item):
    logger.info("Parsing item: %s", item)
    found_podcast = False
    new = False
    podcast_link = None
    # Find audio mimetype to be sure it is a podcast if not skip it
    for link in item['links']:
        if 'type' in link and "audio" in link['type']:
            found_podcast = True
            podcast_link = link
            break
    if found_podcast:
        new_item = RssItem.query.filter_by(enclosure_url=podcast_link['href']).one_or_none()
        if not new_item:
            new = True
            new_item = RssItem(feed)
        if not new:
            logger.info("Updating item: " + str(new_item.id))
        new_item.title = item.get("title")
        new_item.link = item.get("link")
        new_item.description = item.get("description")
        if item.get("published_parsed"):
            logger.debug(f"Found published parsed for podcast {item.get('published_parsed')}")
            new_item.pub_date = datetime.fromtimestamp(mktime(item.get("published_parsed")))
        new_item.itunes_summary = item.get("summary")
        new_item.author = item.get("author")
        new_item.subtitle = item.get("subtitle")
        new_item.itunes_duration = item.get("itunes_duration")
        if "image" in item:
            new_item.itunes_image_url = item.get("image").get("href")
        new_item.guid = item.get("id")
        new_item.enclosure_mime_type = podcast_link['type']
        new_item.enclosure_url = podcast_link['href']
        new_item.length = podcast_link['length']
        podcast = None
        if new or not new_item.podcast_guid:
            logger.info("Creating a new podcast for item: %s", new_item.item_guid)
            # create podcast
            podcast = create_podcast(new_item.title, new_item.description, new_item.author, new_item.language,
                                     feed.feed_guid, new_item.pub_date, from_rss_feed=True)
        if new and podcast:
            logger.info("Created a new podcast for item")
            new_item.podcast_guid = podcast.get("guid")
            db.session.add(new_item)
        elif new and not podcast:
            logger.error("Failed to create podcast for rss feed: %s", feed.feed_guid)
        db.session.commit()
        if new_item.id and new_item.podcast_guid:
            # Refresh files
            process_rss_item.apply_async(args=[new_item.id])


@celery.task
def process_rss_item(item_id):
    from rss import create_app
    app = create_app(config_name)
    with app.app_context():
        logger.debug("Looking for rss item with id of: %s", str(item_id))
        item: RssItem = RssItem.query.get(item_id)
        if item:
            logger.info('Processing item: %s', item.item_guid)
            podcast = get_podcast_info(item.podcast_guid)
            if podcast:
                files = podcast.get("files", [])
                if len(files) == 0:  # TODO check for qualities maybe?
                    local_filename = item.enclosure_url.rsplit('?', 1)[0].rsplit('/', 1)[1]
                    # TODO may need to do something else if no extension exists, currently file service checks for extension to be valid
                    if not local_filename:
                        local_filename = str(uuid.uuid4()).replace('-', '')
                    path_to_file = os.path.join(current_app.config['TEMP_FOLDER'], local_filename)
                    path_to_file = download_file(item.enclosure_url, path_to_file)
                    if path_to_file:
                        podcast_file = upload_podcast(local_filename, path_to_file, item.podcast_guid)
                        if not podcast_file:
                            logger.error("Failed to upload podcast to podcast service")
                        os.remove(path_to_file)
                        db.session.commit()
                    else:
                        logger.error("Failed to download file from: %s", item.enclosure_url)
                else:
                    logger.info("Item: %s already has a file associated with it, skipping upload", item.item_guid)
            else:
                logger.error("Failed to process rss item, could not find podcast for item: %s", item.item_guid)
        else:
            logger.error("Could not find rss item with id: " + str(item_id))


def upload_file_to_url(path_to_file, upload_url) -> Optional[str]:
    logger.info("Uploading file... " + path_to_file)
    local_filename = os.path.basename(path_to_file)
    files = {'file': (local_filename, open(path_to_file, 'rb'), {'Expires': '0'})}
    response = requests.put(upload_url, files=files, headers=current_app.config['DEFAULT_HEADERS'])
    if response.status_code == 201 or response.status_code == 200:
        resp_json = response.json()
        file_guid = resp_json.get("guid_file_name")
        if file_guid:
            return file_guid
    else:
        logger.info(response.content)
        return None


def process_image(feed: RssFeed):
    logger.info("Processing image")
    if feed.image_guid:
        logger.info("Feed already has an image downloaded")
        return
    resp = requests.get(current_app.config['MARSHALLER_GET_UPLOAD_URL'], current_app.config['DEFAULT_HEADERS'])
    if resp.status_code == 200 and resp.json() and resp.json().get('url'):
        upload_url = resp.json().get('url')
        local_filename = feed.image_url.rsplit('?', 1)[0].rsplit('/', 1)[1]
        # TODO may need to do something else if no extension exists, currently file service checks for extension to be valid
        if not local_filename:
            local_filename = str(uuid.uuid4()).replace('-', '')
        path_to_file = os.path.join(current_app.config['TEMP_FOLDER'], local_filename)
        path_to_file = download_file(feed.image_url, path_to_file)
        if path_to_file:
            feed.image_guid = upload_file_to_url(path_to_file, upload_url)
            os.remove(path_to_file)
        else:
            logger.error("Failed to download file from: %s", feed.image_url)
    else:
        logger.error("Error getting upload url for file")


if __name__ == '__main__':
    from rss import create_app
    app = create_app(config_name)
    with app.app_context():
        logger.info("Updating feed: " + str(2))
        parse_feed(2)
