import os
from typing import List
from flask import Blueprint, request, current_app as app, g
from listenup_common.auth import token_auth, token_optional_auth
from listenup_common.api_response import ApiException, ApiResult
from listenup_common.logging import get_logger
from listenup_common.wrappers.podcast import get_podcast_info, get_podcasts_info_by_guids
from listenup_common.wrappers.channel import get_channel_by_url, create_channel
from listenup_common.utils import require_json
from rss.models import RssFeed, RssItem
from rss.tasks import update_feed
from . import db

logger = get_logger(__name__)

main = Blueprint('main', __name__, url_prefix='/api/rss')

root_path = os.path.dirname(os.path.abspath(__file__))


@main.route('/add', methods=['POST'])
@token_auth.login_required
@require_json(required_attrs=['feed_url'])
def add_rss_feed(json):
    logger.info("Adding new rss feed")
    channel = get_channel_by_url(json['feed_url'])
    if not channel:
        logger.info("Creating a new channel for feed url: %s", json['feed_url'])
        channel = create_channel({"feed_url": json['feed_url'], "from_rss_feed": True})
    if not channel:
        raise ApiException("Failed to create rss channel for: " + json['feed_url'])

    logger.info("Updating feed guid: %s", channel['guid'])
    update_feed.apply_async(args=[channel['guid']])
    return ApiResult(value=channel, status=201).to_response()


@main.route('/all', methods=['GET'])
@token_auth.login_required
def get_all_feeds():
    logger.info("Getting all feeds")
    feeds = RssFeed.query.all()
    logger.debug("Returning feeds count: %s", str(len(feeds)))
    return ApiResult(value=[feed.to_dict() for feed in feeds], status=200).to_response()


@main.route('/<string:guid>/items')
@token_optional_auth.login_required
def get_items_for_feed(guid):
    logger.info("Querying for feed guid: %s", guid)
    feed: RssFeed = RssFeed.query.filter_by(feed_guid=guid).one_or_none()
    items: List[RssItem] = RssItem.query.filter_by(feed_guid=guid).all()
    podcasts = get_podcasts_info_by_guids([item.podcast_guid for item in items], internal=True)
    if feed and items and podcasts:
        if request.args.get('channel_style') == "true":
            logger.info("Returning items in channel style for: %s", feed.feed_guid)
            return ApiResult(value=feed.to_dict(channel_style=True, podcasts=podcasts), status=200).to_response()
        else:
            logger.info("Returning items in rss item style for: %s", feed.feed_guid)
            return ApiResult(value=[item.to_dict(podcasts[item.podcast_guid]) for item in items], status=200).to_response()
    else:
        raise ApiException(message="Could not find items or podcasts for feed: " + guid, status=404)


@main.route('/<string:guid>')
@token_optional_auth.login_required
def get_feed(guid):
    item = RssFeed.query.filter_by(feed_guid=guid).one_or_none()
    if item:
        return ApiResult(value=item.to_dict(), status=200).to_response()
    else:
        raise ApiException(message="Could not find feed: " + guid, status=404)


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
