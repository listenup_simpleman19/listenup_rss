# global Celery options that apply to all configurations

# enable the pickle serializer
task_serializer = 'pickle'
result_serializer = 'pickle'
accept_content = ['pickle']
worker_concurrency = 4
